from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait


def test_forgot_password():
    # підключає драйвер браузера. вказуємо шлях де він лежить
    driver = WebDriver(executable_path="D:\Selenium\chromedriver.exe")
    # відкриває ЮРЛ
    driver.get("https://search.dev.myscholly.com/login")

    search_button = driver.find_element_by_xpath("//*[contains(text(), 'Forgot password?')]")
    search_button.click()
    search_input = driver.find_element_by_xpath("//input[@placeholder='Email']")
    search_input.send_keys('nazarpedan315@gmail.com')
    search_submit = driver.find_element_by_xpath("//*[contains(text(), 'Next')]")
    search_submit.click()
    search_sign_up = driver.find_element_by_xpath("//*[contains(text(), 'Sign up')]")
    search_sign_up.click()

    # WebDriverWait(driver, 10, 0.5)

